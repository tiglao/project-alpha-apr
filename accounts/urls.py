from django.urls import path
from accounts.views import login, exit, signup


urlpatterns = [
    path("login/", login, name="login"),
    path("logout/", exit, name="logout"),
    path("signup/", signup, name="signup"),
]
