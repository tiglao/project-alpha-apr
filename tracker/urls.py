from django.contrib import admin
from django.urls import path, include
from projects.views import project_list


# register url for projects urls

urlpatterns = [
    path("", project_list, name="home"),
    path("", include("projects.urls")),
    path("projects/", include("projects.urls")),
    path("accounts/", include("accounts.urls")),
    path("tasks/", include("tasks.urls")),
    path("admin/", admin.site.urls),
]
